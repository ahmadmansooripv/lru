#ifndef LRUALGORITHM_H
#define LRUALGORITHM_H

#include <QAbstractTableModel>
#include<QVector>
#include<QDebug>


class LruAlgorithm : public QAbstractTableModel
{
    Q_OBJECT

public:
    explicit LruAlgorithm(QObject *parent = nullptr);

    // Header:
    QVariant headerData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const override;

    bool setHeaderData(int section, Qt::Orientation orientation, const QVariant &value, int role = Qt::EditRole) override;

    // Basic functionality:
    int rowCount(const QModelIndex &parent = QModelIndex()) const override;
    int columnCount(const QModelIndex &parent = QModelIndex()) const override;

    QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const override;
    QHash<int,QByteArray> roleNames() const override;

    // Editable:
    bool setData(const QModelIndex &index, const QVariant &value,
                 int role = Qt::EditRole) override;

    Qt::ItemFlags flags(const QModelIndex& index) const override;

    // Add data:
    bool insertRows(int row, int count, const QModelIndex &parent = QModelIndex()) override;
    bool insertColumns(int column, int count, const QModelIndex &parent = QModelIndex()) override;

    // Remove data:
    bool removeRows(int row, int count, const QModelIndex &parent = QModelIndex()) override;
    bool removeColumns(int column, int count, const QModelIndex &parent = QModelIndex()) override;
    Q_INVOKABLE void addCol(QVariant value);
    Q_INVOKABLE void changeRow(QVariant value);
    Q_INVOKABLE void solveLru();

private:
    struct state{
        int tag;
        int data;
        bool isFault;
    };

    QVector<int> frames;
    int frameNumber;
    int m_fault;
    QVector<QVector<state>> result;
};

#endif // LRUALGORITHM_H
