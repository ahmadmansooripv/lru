#include "lrualgorithm.h"

LruAlgorithm::LruAlgorithm(QObject *parent)
    : QAbstractTableModel(parent)
{

    frameNumber=2;
    ////    QVector<state> current_frames;
    //    for(int i =0;i<frames.size();i++){
    //        struct state current;
    //        current.tag = 0;
    //        current.isFault = false;
    //        current.data = -1;
    //        result.append(current);
    //    }

}

QVariant LruAlgorithm::headerData(int section, Qt::Orientation orientation, int role) const
{
    // FIXME: Implement me!
}

bool LruAlgorithm::setHeaderData(int section, Qt::Orientation orientation, const QVariant &value, int role)
{
    if (value != headerData(section, orientation, role)) {
        // FIXME: Implement me!
        emit headerDataChanged(orientation, section, section);
        return true;
    }
    return false;
}


int LruAlgorithm::rowCount(const QModelIndex &parent) const
{
    if (parent.isValid())
        return frameNumber;

    return frameNumber;
}

int LruAlgorithm::columnCount(const QModelIndex &parent) const
{
    if (parent.isValid())
        return frames.size();
    return  frames.size();
}

QVariant LruAlgorithm::data(const QModelIndex &index, int role) const
{

    if (!index.isValid())
        return QVariant();
    if (result.size() <=0){
        return QVariant();
    }
    if (result.size() < frames.size() || result[0].size() >frameNumber)
        return QVariant();
    int data = result[index.column()][index.row()].data;
    bool isFault = result[index.column()][index.row()].isFault;
    if (data != -1){
        if (role == 0){
            return QVariant(data);
        }
        else if(role ==1){
            return QVariant(isFault);
        }
    }
    else
        return QVariant();
}

QHash<int, QByteArray> LruAlgorithm::roleNames() const
{
    QHash<int,QByteArray> roles;
    roles[0] = "data";
    roles[1] = "isFault";
    return roles;
}

bool LruAlgorithm::setData(const QModelIndex &index, const QVariant &value, int role)
{
    if (data(index, role) != value) {
        // FIXME: Implement me!
        emit dataChanged(index, index, QVector<int>() << role);
        return true;
    }
    return false;
}

Qt::ItemFlags LruAlgorithm::flags(const QModelIndex &index) const
{
    if (!index.isValid())
        return Qt::NoItemFlags;

    return Qt::ItemIsEditable; // FIXME: Implement me!
}

bool LruAlgorithm::insertRows(int row, int count, const QModelIndex &parent)
{
    beginInsertRows(parent, row, row + count - 1);
    // FIXME: Implement me!
    endInsertRows();
}

bool LruAlgorithm::insertColumns(int column, int count, const QModelIndex &parent)
{
    beginInsertColumns(parent, column, column + count - 1);
    // FIXME: Implement me!
    endInsertColumns();
}

bool LruAlgorithm::removeRows(int row, int count, const QModelIndex &parent)
{
    beginRemoveRows(parent, row, row + count - 1);
    // FIXME: Implement me!
    endRemoveRows();
}

bool LruAlgorithm::removeColumns(int column, int count, const QModelIndex &parent)
{
    beginRemoveColumns(parent, column, column + count - 1);
    // FIXME: Implement me!
    endRemoveColumns();
}

void LruAlgorithm::addCol(QVariant value)
{
    frames<<value.toInt();
    insertColumns(frames.size(),1);
//    qDebug()<<"column added";
    //    insertRows(frames.size(),1);
}

void LruAlgorithm::changeRow(QVariant value)
{
        qDebug()<<value.toInt()<<" "<<frameNumber;
    if (value.toInt() > frameNumber)
    {
        insertRows(frameNumber,value.toInt() - frameNumber);
        frameNumber = value.toInt();
    }
    else if (value.toInt()<frameNumber){
        removeRows(frameNumber,frameNumber - value.toInt());
        frameNumber = value.toInt();
    }

    else if (value.toInt() == frameNumber){
        removeRows(frameNumber,frameNumber - value.toInt());
//        emit dataChanged(index(0,0),index(frameNumber,frames.size()),QVector<int>()<<0<<1);
    }
}

void LruAlgorithm::solveLru()
{
    QVector<QVector<state>> temp;
    bool next = false;
    int oldest =0;
    QVector<state> current_frame;
    for(int i =0;i<frameNumber;i++){
        struct state current;
        current.tag = 0;
        current.data = -1;
        current.isFault = false;
        current_frame.append(current);
    }
    for(int i=0;i<frames.size();i++){
//        for(auto& a:temp){
//            QString s="";
//            for(auto& b:a){
//                s+="("+QVariant(b.data).toString()+" "+QVariant(b.tag).toString()+") ";
//            }
//            qDebug()<<s;
//        }
//        qDebug()<<endl;
        int oldest_tag=0;
        for(int j=0;j<frameNumber;j++){
            current_frame[j].isFault = false;
            current_frame[j].tag++;
            if (current_frame[j].tag>oldest_tag){
                oldest = j;
                oldest_tag = current_frame[j].tag;
            }
        }
        for(int j=0;j<frameNumber;j++){

            //            oldest = result[j].tag;
            if (frames[i] == current_frame[j].data || current_frame[j].data == -1){
                current_frame[j].tag = 0;
                current_frame[j].isFault = false;
                current_frame[j].data = frames[i];
                next = true;
                break;
            }
        }
        if (next){
            temp.append(current_frame);
            next = false;
            continue;
        }
        else{
            current_frame[oldest].tag = 0;
            current_frame[oldest].data = frames[i];
            current_frame[oldest].isFault = true;
            temp.append(current_frame);

        }

    }
    result = temp;
    emit dataChanged(index(0,0),index(frameNumber,result.size()),QVector<int>()<<0<<1);
}
