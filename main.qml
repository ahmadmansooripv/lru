import QtQuick 2.12
import QtQuick.Window 2.12
import QtQuick.Controls 2.5
import QtQuick.Controls.Material 2.3
import QtQuick.Layouts 1.3
import core.backend 1.0

ApplicationWindow {
    id:mainwindow
    visible: true
    width: screen.width
    height: screen.height
    title: qsTr("LRU")

    Material.theme: Material.Dark

    Component.onCompleted: {
        numberinput.focus = true
    }


   RowLayout{
    id:rowl
    width: parent.width/4
    height: parent.height*(1/4)
    anchors.horizontalCenter: parent.horizontalCenter

    TextField{
        Layout.topMargin: screen.height/10
        id:numberinput
        placeholderText: "enter frame number"
        anchors.verticalCenter: numberenter.verticalCenter
        Layout.preferredWidth: parent.width*(1/4)
//        Layout.leftMargin: width/6


    }
    RoundButton{
        id:numberenter
        Layout.preferredWidth: parent.width*(1/6)
        Layout.preferredHeight: parent.width*(1/6)
        Layout.leftMargin: parent.width/10
//        Shortcut{
//            sequence: Qt.Key_Enter
//            onActivated: {
//                if (numberinput.text !=""){
//                    lru.addCol(numberinput.text);
//                    lbl.text += " "+numberinput.text
//                    numberinput.text = ""
//                    numberinput.focus = true
//                }
//            }
//        }

        Material.background: "#009688"
        text:"enter"
        onClicked: {
            if (numberinput.text !=""){
                lru.addCol(numberinput.text);
                lbl.text += " "+numberinput.text
                numberinput.text = ""
                numberinput.focus = true
            }
        }

    }

    TextField{
        Layout.topMargin: screen.height/10
        id:frameinput
        placeholderText: "nter number of frame"
        anchors.verticalCenter: numberenter.verticalCenter
        Layout.preferredWidth: parent.width*(1/4)
        Layout.leftMargin: width/6

    }

    RoundButton{
        id:frameenter
        Layout.preferredWidth: parent.width*(1/6)
        Layout.preferredHeight: parent.width*(1/6)
        Layout.leftMargin: parent.width/15
        Material.background: "#009688"
        Layout.alignment: Qt.AlignRight
        text:"solve"
        onClicked: {
            if (frameinput.text.trim()!=""){
            lru.changeRow(frameinput.text);
            lru.solveLru();
            }
            else{
                frameinput.placeholderText = "Frame number is neccessary"
            }
        }

    }

    }
   Label{
       id:lbl
       text: ""
//        Layout.leftMargin: rowl.width/10
       anchors.top: tablemodel.bottom
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.topMargin: screen.height/6
        font.bold: true
        font.pixelSize: 20
   }
   TableView{
       id:tablemodel
       width: parent.width/2
       height: parent.height*(2/4)
       anchors.horizontalCenter: parent.horizontalCenter
       anchors.verticalCenter: parent.verticalCenter
       anchors.top:rowl.bottom
        clip: true
        model: LRU{id : lru}
        rowSpacing: 3
        columnSpacing: 3

       delegate: Button{
           text:model.data
           Material.background: model.isFault ? "red" : "grey"
           anchors.verticalCenter:mainwindow.verticalCenter
           onClicked: {
               if(model.isFault){
                   console.log("fault");
               }
           }
       }
   }



}
